Name:           python-jmespath
Version:        1.0.1
Release:        1
Summary:        Matching Expressions for JSON
License:        MIT
URL:            https://github.com/jmespath/jmespath.py
Source0:        https://files.pythonhosted.org/packages/00/2a/e867e8531cf3e36b41201936b7fa7ba7b5702dbef42922193f05c8976cd6/jmespath-1.0.1.tar.gz
BuildArch:      noarch

%description
JMESPath is a python library which allows you to declaratively specify how to
extract elements from a JSON document.

%package -n     python3-jmespath
Summary:        JSON Matching Expressions
%{?python_provide:%python_provide python3-jmespath}

BuildRequires:  python3-devel python3-mock python3-setuptools python3-pytest

%description -n python3-jmespath
JMESPath is a python library which allows you to declaratively specify how to
extract elements from a JSON document.

%prep
%setup -q -n jmespath-%{version}
rm -rf jmespath.egg-info

%build
%py3_build


%install
%py3_install
mv %{buildroot}/%{_bindir}/jp.py %{buildroot}/%{_bindir}/jp.py-%{python3_version}
ln -sf %{_bindir}/jp.py-%{python3_version} %{buildroot}/%{_bindir}/jp.py-3

%check
python3 -m unittest discover -v

%files -n python3-jmespath
%doc README.rst LICENSE.txt
%{_bindir}/jp.py-3
%{_bindir}/jp.py-%{python3_version}
%{python3_sitelib}/jmespath*

%changelog
* Fri Mar 31 2023 wubijie <wubijie@kylinos.cn> - 1.0.1-1
- Update package to version 1.0.1

* Fri Apr 29 2022 houyingchao <houyingchao@h-partners.com> - 0.10.0-2
- Remove dependency on python-nose

* Wed Jul 14 2021 huangtianhua <huangtianhua@huawei.com> - 0.10.0-1
- Upgrade to 0.10.0 to support OpenStack-W

* Thu Nov 19 2020 caodongxia <caodongxia@huawei.com> - 0.9.0-14
- Remove python2 subPackage

* Sat Nov 16 2019 yanzhihua <yanzhihua4@huawei.com> - 0.9.0-13
- Package init

